const express = require("express");

const app = new express();

const port = 8000;

class Drink {
    _maNuoc;
    _tenNuoc;
    _donGia;
    _ngayTao;
    _ngayCapNhat;

    constructor(paramMa, paramTen, paramDonGia, paramNgayTao, paramNgayCapNhat) {
        this._maNuoc = paramMa;
        this._tenNuoc = paramTen;
        this._donGia = paramDonGia;
        this._ngayTao = paramNgayTao;
        this._ngayCapNhat = paramNgayCapNhat;
    }

    getCode() {
        return this._maNuoc;
    }
}

var vDrinkList = [
    new Drink("TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021"),
    new Drink("COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021"),
    new Drink("PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021"),
];

app.get("/drinks-class", (req, res) => {
    let query = req.query;
    let code = query.code;
    if (code == null || code == "") {
        res.json({
            drinkList: vDrinkList
        });
    } else {
        let drinkListFiltered = vDrinkList.filter((drink) => { return drink.getCode() == code });
        res.json({
            drinkListFiltered
        });
    }
});

app.get("/drinks-class/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    if (drinkId == "") {
        res.json({
            drinkList: vDrinkList
        });
    } else {
        let drinkListFiltered = vDrinkList.filter((drink) => { return drink.getCode() == drinkId });
        res.json({
            drinkListFiltered
        });
    }
});

var vDrinkListObj = [
    {
        maNuoc: "TRATAC",
        tenNuoc: "Trà tắc",
        donGia: 10000,
        ngayTao: "14/5/2021",
        ngayCapNhat: "14/5/2021",
    },
    {
        maNuoc: "COCA",
        tenNuoc: "Cocacola",
        donGia: 15000,
        ngayTao: "14/5/2021",
        ngayCapNhat: "14/5/2021",
    },
    {
        maNuoc: "PEPSI",
        tenNuoc: "Pepsi",
        donGia: 15000,
        ngayTao: "14/5/2021",
        ngayCapNhat: "14/5/2021",
    }
];

app.get("/drinks-object", (req, res) => {
    let query = req.query;
    let code = query.code;
    if (code == null || code == "") {
        res.json({
            drinkListObj: vDrinkListObj
        });
    } else {
        let drinkListObjFiltered = vDrinkListObj.filter((drink) => { return drink.maNuoc == code });
        res.json({
            drinkListObjFiltered
        });
    }
});

app.get("/drinks-object/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    if (drinkId == "") {
        res.json({
            drinkListObj: vDrinkListObj
        });
    } else {
        let drinkListObjFiltered = vDrinkListObj.filter((drink) => { return drink.maNuoc == drinkId });
        res.json({
            drinkListObjFiltered
        });
    }
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})